var picture;
var name;
var lastName;

function callme(){
window.fbAsyncInit = function() {
    FB.init({
      appId      : '1980314288850431',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView(); 
    loadsdk();
    checkLoginState();
};
}


function onLogin(response) {
	  if (response.status == 'connected') {
	    FB.api('/me?fields=name', function(data) {
	      var welcomeBlock = document.getElementById('fb-welcome');
	      welcomeBlock.innerHTML = 'Hello, ' + data.name + '!';
	      
	    });
	  }
};

	
function loadsdk(){
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
};



function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
};

var user_id;
//This function is called with in the results from FB.getLoginStatus()
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
      user_id = response.authResponse.userID;
      console.log(user_id);
      extractInfo();
      console.log("Already LoggedIn");
    }
    /*else if (response.status === 'not_authorized') {
    	//person is logged in facebook but not in app.
		document.getElementById('status').innerHTML = 'Please log ' + 'into this app';

    }*/
	else{
        document.getElementById('status').innerHTML = 'Please log ' + 'into facebook';
    	console.log("Please login");
      FB.login();
    }
  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=900799546729806";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

function shareTweet(){
	checkLoginState();
	FB.ui({method: 'share',
		href: document.getElementById("status").value,
		quote: document.getElementById('text_content').value,
		},function(response){
		if (!response || response.error)
		{
			console.log(response.error);
			alert('Posting error occurred');
		}
	});
	
};


function extractInfo(){
	FB.api('/me',
            'GET',
             {"fields":"id,first_name,last_name"},
			function(response){
				name = response.first_name;
                lastName = response.last_name;
                console.log(response);
                console.log('successful get /me from API with name: '+response.first_name);
				document.cookie="user_id="+response.id;
				document.cookie="name="+name;
                document.cookie="last_name="+lastName;

                document.getElementById("user_id").value=response.id;
                document.getElementById("name").value =name;
                document.getElementById("last_name").value = lastName;

                console.log('FB call'+document.getElementById("last_name").value);
                localStorage.setItem('name',name);
                localStorage.setItem('last_name',name);
				console.log(document.cookie);
			});
	 FB.api(
			  '/me/picture',
			  'GET',
			  {"height":"100"},
			  function(response) {
				  if(response && !response.error) {
                      picture = "<img src='" + response.data.url + "'>";
                      document.cookie = "picture=" + picture;
                      console.log('successful /me/picture from API: '+response.data.url);
                      document.getElementById("picture").value = picture;

                      console.log('FB call picture'+document.getElementById("picture").value);

                      localStorage.setItem('picture', picture);
                      console.log(document.cookie);
                  }else {
				  	console.log('Error in getting picture')
				  }
			  }
			);
	if(document.getElementById("user_id").value == null) {
	   document.getElementById("user_id").value    = getCookie('user_id');
       document.getElementById("name").value = getCookie('name');
       document.getElementById("last_name").value = getCookie('last_name');
    }

    if(document.getElementById("picture").value == null) {
        document.getElementById("picture").value = getCookie('picture');
    }
    console.log(document.cookie);
};


function getCookie(cname) {
	var re = new RegExp(cname + "=([^;]+)");
	var value = re.exec(document.cookie);
	return (value != null) ? unescape(value[1]) : null;
}


function sendDirectMsg(){
	checkLoginState();
	FB.ui({method:  'send',
		  link: document.getElementById("status").value,});
	console.log('status'+document.getElementById("status"));
};


