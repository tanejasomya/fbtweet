<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.DatastoreService"%>
<%@ page
	import="com.google.appengine.api.datastore.DatastoreServiceFactory"%>
<%@ page import="com.google.appengine.api.datastore.Entity"%>
<%@ page import="com.google.appengine.api.datastore.KeyFactory"%>
<%@ page import="com.google.appengine.api.datastore.Query.FilterOperator"%>
<%@ page import="com.google.appengine.api.datastore.Query.FilterPredicate"%>
<%@ page import="com.google.appengine.api.datastore.Query"%>
<%@ page import="com.google.appengine.api.datastore.PreparedQuery"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="/css/tweet.css">
<script type="text/javascript" src="/js/tweet.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<div class="topnav">
		<a href="tweet.jsp">Tweet</a>
		<a href="getmytweet.jsp">My Dashboard</a>
		<a  id=toptweet href="toptweet.jsp">Hot Tweet</a>
		<a href="friendstweet.jsp">Friends</a>
		<div id="fb-root"></div>
		<div align="right">
			<div class="fb-login-button" data-max-rows="1" data-size="large"
				data-button-type="login_with" data-show-faces="false"
				data-auto-logout-link="true" data-use-continue-as="true"
				scope="public_profile,email" onlogin="checkLoginState();"></div>
		</div>
	</div>


</body>
</html>

<%
	String userId = null;
	Cookie cookie = null;
	Cookie[] cookies = null;
	cookies = request.getCookies();
	if( cookies != null)
	{
		for (int i = 0; i < cookies.length; i++){
			cookie = cookies[i];
			if("user_id".equals(cookie.getName())){
				userId = cookie.getValue();
			}
		}
	}

	if(userId == null){
	    userId = (String) session.getAttribute("backup-user");
		//out.println("Error as userId is empty");
	}

	if(userId == null){
		out.println("Error as userId is empty");
	}

	DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
	Entity e = new Entity("tweet");
	Query q = new Query("tweet");
	q.setFilter(new FilterPredicate("user_id", FilterOperator.EQUAL, userId));
	PreparedQuery pq = ds.prepare(q);

	for (Entity result : pq.asIterable()) {
		if (result.getProperty("user_id") != null) {
			//String name = (String) result.getProperty("name");
			//String lastName = (String) result.getProperty("last_name");
			String user_id = (String) result.getProperty("user_id");
			String picture = (String) result.getProperty("picture");
			String status = (String) result.getProperty("status");
			Long id = (Long) result.getKey().getId();
			String time = (String) result.getProperty("timestamp");
			Long visited_count = (Long) ((result.getProperty("visited_count")));
			//StringBuffer sb = new StringBuffer();
			//String url = request.getRequestURL().toString();
			//String baseURL = url.substring(0, url.length() - request.getRequestURI().length())+ request.getContextPath() + "/";
			//sb.append(baseURL + "directTweet.jsp?id=" + id);
%>
<div align="left" class="tweet">
<table>
	<tr>
		<td width="100px"><%= picture %></td>
		<td width="350px">Status: <%= status %></td>
		<td width="250px">Posted at: <%=time %></td>
		<td width="125px">Visited : <%= visited_count %></td>
	</tr>
	<tr>
		<form action="getmytweet.jsp" action="GET">
			<input type=hidden name=user_id id=user_id value=<%=user_id%> />
			<input type=hidden name=u_id id=u_id value=<%=id%> />
			<td><button name="Delete" type="submit" class="button" value=Delete >Delete</button></td>
		</form>
	</tr>
  </table>
</div>
<%
	/*Entity s = ds.get(KeyFactory.createKey("tweet", id));
			s.setProperty("visited_count", visited_count + 1);
			ds.put(s);*/
		}
	}
	if (request.getParameter("u_id") != null) {
		Entity s = ds.get(KeyFactory.createKey("tweet", Long.parseLong(request.getParameter("u_id"))));
		ds.delete(s.getKey());
		out.println("STATUS with id:" + Long.parseLong(request.getParameter("u_id")) + " deleted from Tweet Store");
	}
%>
