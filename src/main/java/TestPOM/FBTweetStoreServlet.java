package TestPOM;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;


@SuppressWarnings("serial")
public class FBTweetStoreServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(FBTweetStoreServlet.class.getName());
 private static final DateFormat SDF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

		DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
		Entity e=new Entity("tweet");
		e.setProperty("status",req.getParameter("text_content"));
		e.setProperty("user_id", req.getParameter("user_id"));
		e.setProperty("name", req.getParameter("name"));
		e.setProperty("last_name", req.getParameter("last_name"));
		e.setProperty("picture", req.getParameter("picture"));
		e.setProperty("visited_count", 0);

		log.info("user_id is "+req.getParameter("user_id"));
		log.info("name is "+req.getParameter("name"));
		log.info("visited_count is "+req.getParameter("visited_count"));
		log.info("picture is "+req.getParameter("picture"));
		log.info("last_name"+ req.getParameter("last_name"));

		//Setting the needed values in cookie for further use
		Cookie userId = new Cookie("user_id", req.getParameter("user_id"));
		resp.addCookie(userId);

		Cookie name= new Cookie("name",req.getParameter("name"));
		resp.addCookie(name);

		Cookie lastName =new Cookie("last_name", req.getParameter("last_name"));
		resp.addCookie(lastName);

		Cookie pic = new Cookie("picture", req.getParameter("picture"));
		resp.addCookie(pic);

		Date date = new Date();
		e.setProperty("timestamp", SDF.format(date));
		Key id=ds.put(e);		
		StringBuilder sb=new StringBuilder();
		String url = req.getRequestURL().toString();
		String baseURL = url.substring(0, url.length() - req.getRequestURI().length()) + req.getContextPath() + "/";
		sb.append(baseURL+"directTweet.jsp?id="+id.getId());
		req.setAttribute("status", sb);
		req.getRequestDispatcher("tweet.jsp").forward(req, resp);
	}
}
