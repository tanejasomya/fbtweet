<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.logging.Logger" %>
<%@ page import="com.google.appengine.api.datastore.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/tweet.css">
</head>
<body>
 <script type="text/javascript" src="/js/tweet.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<div class="topnav">
  <a href="tweet.jsp">Tweet</a>
  <a href="getmytweet.jsp">My Dashboard</a>
  <a  id=toptweet href="toptweet.jsp">Hot Tweet</a>
  <a href="friendstweet.jsp">Friends</a>

  <div id="fb-root"></div>
  <div align="right">
  <div class="fb-login-button" data-max-rows="1"    data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true"  data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();"></div>
  </div>
</div>
<script type="text/javascript">callme()</script>

 <%
   if(null != request.getParameter("status")){
     request.setAttribute("status",request.getAttribute("status"));
   }
 %>

<input type=hidden id=status value="${status}">
<br><div align="left">
<table>
<tr>
<form id="tweetStore" action="tweetStore" method="get" name="tweetStore" >
<td><textarea id="text_content" name="text_content" class="textarea"
							placeholder="What's on your mind"></textarea></td>
<input type=hidden id=user_id name= user_id />
<input type=hidden id=name name=name  />
<input type=hidden id=last_name name=last_name  />
<input type=hidden id=picture name=picture  />

<td><input type="submit" id=submit name=save class="button" value="Tweet"/>
</form>
<br><input type="button"  id="create_tweet" class="button" value="Share Tweet" />

</td>
</tr>
</table>
</div>


<div align="right">

<div id="mypopup" class="popup">
<div  class="popup-content">
<span class="close">&times;</span>
<input type="button"  class="button" value="Post Tweet" name="share_tweet" onclick=shareTweet() />
<input type="button"  class="button" value="Send Direct Message" name="send_direct_msg" onclick=sendDirectMsg() />
</div>
</div>

</div>


<script>

var modal = document.getElementById('mypopup');
var btn = document.getElementById("create_tweet");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
    modal.style.display = "block";
};
span.onclick = function() {
    modal.style.display = "none";
};
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
};
document.getElementById("user_id").value  = getCookie('user_id');
document.getElementById("name").value = getCookie('name');
document.getElementById("last_name").value = getCookie('last_name');
document.getElementById("picture").value    = getCookie('picture');
document.getElementById("toptweet").href="toptweet.jsp?name="+getCookie("name");

</script>

</body>
</html>

<div align="left" style="font-size: larger"><b>What People are saying:</b></div>
<br>
<div align="left" class="tweet">
<%
  DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
  Entity e=new Entity("tweet");
  Query q=new Query("tweet");
  q.addSort("timestamp", Query.SortDirection.DESCENDING);
  PreparedQuery pq = ds.prepare(q);
  int count=0;
  for (Entity result : pq.asIterable()) {
    String name = (String) result.getProperty("name");
    String lastName = (String) result.getProperty("last_name");
    String picture = (String) result.getProperty("picture");
    String status = (String) result.getProperty("status");
    Long id = (Long) result.getKey().getId();
    String time = (String) result.getProperty("timestamp");
    Long visited_count = (Long)((result.getProperty("visited_count"))); %>

  <table>
    <tbody>
    <tr style="font-size: large"> <%= name %>&nbsp;<%=lastName%> says: </tr>
    <tr><td width="100px"><%= picture %></td>
    <td width="450px">Status: <%= status %></td>
    <td width="300px">Posted at: <%=time %></td>
    </tbody>
  </table>
  <br>
<%
  // Patch - this is to handle getmytweet incase cookie is not build yet or corrupted
  if(session!=null && session.getAttribute("backup-user")!=null) {
    String userId = null;
    Cookie cookie = null;
    Cookie[] cookies = null;
    cookies = request.getCookies();
    if (cookies != null) {
      for (int i = 0; i < cookies.length; i++) {
        cookie = cookies[i];
        if ("user_id".equals(cookie.getName())) {
          userId = cookie.getValue();
          session.setAttribute("backup-user", userId);
        }
      }
    }
  }
}
%>

</div>
