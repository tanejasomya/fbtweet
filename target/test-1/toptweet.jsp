<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.google.appengine.api.datastore.Query.SortDirection" %>
<%@ page import="com.google.appengine.api.datastore.*" %>
<%@ page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="/css/tweet.css">
<title>Top Tweets</title>
</head>
<body>
 <script type="text/javascript" src="/js/tweet.js"></script>
 <script> callme();</script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<div class="topnav">
	<a href="tweet.jsp">Tweet</a>
	<a href="getmytweet.jsp">My Dashboard</a>
	<a  id=toptweet href="toptweet.jsp">Hot Tweet</a>
	<a href="friendstweet.jsp">Friends</a>
  <div id="fb-root"></div>
  <div align="right">
  <div class="fb-login-button" data-max-rows="1"    data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true"  data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();"></div>
  </div>
</div>
<h2>Hot Tweets: </h2>
</body>
</html>
<div align="left" class="tweet">
	<table>
		<tbody>
<%
	DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
	Entity e=new Entity("tweet");
	Query q=new Query("tweet").addSort("visited_count", SortDirection.DESCENDING);
	PreparedQuery pq = ds.prepare(q);
	int count=0;
	for (Entity result : pq.asList(FetchOptions.Builder.withLimit(10))) {
			  String name = (String) result.getProperty("name");
			  String lastName = (String) result.getProperty("last_name");
			  String picture = (String) result.getProperty("picture");
			  String status = (String) result.getProperty("status");
			  Long id = (Long) result.getKey().getId();
			  String time = (String) result.getProperty("timestamp");
			  Long visited_count = (Long)((result.getProperty("visited_count")));
%>


				  <tr>
					  <td width="200px"> <%= name %>&nbsp;<%=lastName%> </td>
					  <td width="100px"> <%= picture %></td>
					  <td width="400px">Status: <%= status %></td>
					  <td width="280px">Posted at: <%=time %></td>
					  <td width="150px">Visited : <%= visited_count %></td>
				  </tr>


			<%  /*Entity s=ds.get(KeyFactory.createKey("tweet", id));
			  s.setProperty("visited_count", visited_count+1);
			  ds.put(s);
			  count++;*/
	}
%> </tbody>
	</table>
	<br>
</div>